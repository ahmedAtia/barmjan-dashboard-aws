import DashboardLayout from "@/layout/dashboard/DashboardLayout.vue";
// GeneralViews
import NotFound from "@/pages/NotFoundPage.vue";

// Admin pages
import Dashboard from "@/pages/Dashboard.vue";
import Profile from "@/pages/Profile.vue";
import Notifications from "@/pages/Notifications.vue";
import Icons from "@/pages/Icons.vue";
import Maps from "@/pages/Maps.vue";
import Typography from "@/pages/Typography.vue";
import TableList from "@/pages/TableList.vue";

import CourseCreation from "../pages/Courses/Create.vue";

import CoursesList from "../pages/Courses/List.vue";

import CourseUpdate from "../pages/Courses/Update.vue";


import TopicCreation from "../pages/Topics/Create.vue";

import TopicsList from "../pages/Topics/List.vue";

import TopicUpdate from "../pages/Topics/Update.vue";



import ChannelCreation from "../pages/Channels/Create.vue";

import ChannelsList from "../pages/Channels/List.vue";

import ChannelUpdate from "../pages/Channels/Update.vue";

import VideosList from "../pages/Videos/List.vue";

import VideoUpdate from "../pages/Videos/Update.vue";

import TeamMemberCreate from "../pages/Team/Create.vue";

import TeamList from "../pages/Team/List.vue";

import TeamMemberUpdate from "../pages/Team/Update.vue";

import Unlock from "../pages/Login/index.vue";

const routes = [
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/dashboard",
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: "dashboard",
        name: "dashboard",
        component: Dashboard,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "team",
        component: TeamList
      },
      {
        path: "team/create",
        name: "teamMemberCreate",
        component: TeamMemberCreate
      },

      {
        path: "team/update/:slug",
        name: "teamMemberUpdate",
        component: TeamMemberUpdate
      },
      {
        path: "courses",
        name: "courses",
        component: CoursesList
      },
      {
        path: "course/create",
        name: "createCourse",
        component: CourseCreation
      },
      {
        path: "course/update/:slug",
        name: "updateCourse",
        component: CourseUpdate
      },

      {
        path: "topics",
        name: "topics",
        component: TopicsList
      },
      {
        path: "topic/create",
        name: "createTopic",
        component: TopicCreation
      },
      {
        path: "topic/update/:slug",
        name: "updateTopic",
        component: TopicUpdate
      },

      {
        path: "channels",

        name: "channels",

        component: ChannelsList
      },
      {
        path: "channels/create",

        name: "createChannels",

        component: ChannelCreation
      },

      {
        path: "channels/update/:slug",

        name: "updateChannels",

        component: ChannelUpdate
      },

      {
        path: "videos",

        name: "videos",

        component: VideosList
      },

      {
        path: "videos/update/:slug",

        name: "updatevideos",

        component: VideoUpdate
      },

      {
        path: "profile",
        name: "profile",
        component: Profile
      },
      {
        path: "notifications",
        name: "notifications",
        component: Notifications
      },
      {
        path: "icons",
        name: "icons",
        component: Icons
      },
      {
        path: "maps",
        name: "maps",
        component: Maps
      },
      {
        path: "typography",
        name: "typography",
        component: Typography
      },
      {
        path: "table-list",
        name: "table-list",
        component: TableList
      }
    ]
  },
  { path: "*", component: NotFound },

  {
    path: "/unlock",
    name: "unlock",
    component: Unlock,
    meta: {
      requiresVisitor: true
    }
  }
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
