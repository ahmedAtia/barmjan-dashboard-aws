import Vue from "vue";

import Vuex from "vuex";
import { resolve, reject } from "any-promise";

Vue.use(Vuex);

const baseUrl = "https://www.barmj.io/api";
// const baseUrl = "http://127.0.0.1:8000/api";

const endPoints = {
  courses : `${baseUrl}/courses/list`,
  topics  : `${baseUrl}/topics/list`,
  channels: `${baseUrl}/channels`,
  login   : `${baseUrl}/auth/login`,
  logout  : `${baseUrl}/auth/logout`
};

function saveToken(data, cb) {
  let access_token = data.access_token;
  localStorage.setItem("access_token", access_token);

  // user is auth ^_^
  cb("AuthUser", access_token);
}

function clearToken(cb) {
  localStorage.removeItem("access_token");

  cb("logout");
}

export const store = new Vuex.Store({
  state: {
    authenticated: false,
    authError    : null,
    access_token : localStorage.getItem("access_token") || null,
    courses      : [],
    channels     : [],
    whole        : [],
    topics       : [],
    wholeTopics  : []
  },

  mutations: {
    AuthUser(state, access_token) {
      state.authenticated = true;
      state.authError = null;
      state.access_token = access_token;
    },

    AuthError(state, e) {
      state.authError = e;
    },

    logout(state) {
      state.authenticated = false;
      state.authError = null;
      state.access_token = null;
    },

    setChannels(state, channels) {
      state.channels = channels;
    },

    filterCourses(state, channel_id) {
      if (!channel_id) {
        state.courses = state.whole;
        return;
      }

      state.courses = _.filter(state.whole, function(course) {
        return course.channel.id == channel_id;
      });
    },

    filterTopics(state, course_id) {
      console.log(course_id);
      if (!course_id) {
        state.topics = state.wholeTopics;
        return;
      }

      state.topics = _.filter(state.wholeTopics, function(topic) {

        return topic.course_id == course_id;
      });

      console.log(state.topics);
    },

    setTopics(state, topics){
      state.topics = topics;
      state.wholeTopics = topics;
    },

    setCourses(state, courses) {
      // let maped =  _.map(courses,(course) =>{

      //     return course[0];

      // } );

      state.courses = courses;

      state.whole = courses;
    }
  },

  actions: {
    destroyToken({ commit, getters }) {
      if (!getters.loggedIn) return;

      return new Promise((resolve, reject) => {
        axios
          .post(endPoints.logout)
          .then(res => {
            clearToken(commit);

            resolve();
          })
          .catch(({ response }) => {
            clearToken(commit);

            reject();
          });
      });
    },

    retrieveToken({ commit }, credentials) {
      return new Promise((resolve, reject) => {
        axios
          .post(endPoints.login, {
            username: credentials.username,
            password: credentials.password
          })

          .then(({ data }) => {
            saveToken(data, commit);

            resolve(data);
          })
          .catch(err => {

            commit("AuthError", err);

            reject(err);
          });
      });
    },

    async fetchChannels(context) {
      try {
        let { data } = await axios.get(endPoints.channels);
        context.commit("setChannels", data.channels);
      } catch (error) {
        dd(error, "iam error here");
      }
    },


    async fetchCourses({ commit }) {
      try {
        let { data } = await axios.get(endPoints.courses);
        commit("setCourses", data.courses);
      } catch (error) {
        dd(error, "iam error here");
      }
    },

    async fetchTopics({ commit }) {
      try {
        let { data } = await axios.get(endPoints.topics);
        commit("setTopics", data.topics);
      } catch (error) {
        dd(error, "iam error here");
      }

    }
  },

  getters: {
    loggedIn    : state => state.access_token != null,
    access_token: state => state.access_token,
    channels    : state => state.channels,
    courses     : state => state.courses,
    whole       : state => state.whole,
    wholeCourses       : state => state.whole,
    topics      : state => state.topics,
    wholeTopics : state => state.wholeTopics,
  }
});
