window.events = new Vue(); // events is just vue instance

window.flash = function(message, level = "success") {
  window.events.$emit("flash", { message, level }); // trager the vue as events to emit flash event
  //which will cause flash component method flash to fire up .
}; // flash('my new flash message')

window.dd = (toDump, explaination) => {
  console.log(explaination);

  console.log(toDump);
};

window.goToByScroll = id => {
  // Remove "link" from the ID
  // id = id.replace("link", "");
  // Scroll

  $("html,body").animate({
    scrollTop: $("#" + id).offset().top
  });
};

window.redirect = (path = "") => {
  window.location = path;
};

window.Gtrans = (key, replace = {}) => {
  let translation = key
    .split(".")
    .reduce((t, i) => t[i] || null, window.translations);

  for (var placeholder in replace) {
    translation = translation.replace(`:${placeholder}`, replace[placeholder]);
  }

  return translation;
};

window.Vue.prototype.isAr = function() {
  if (window.app.lang === "ar") {
    return true;
  }

  return false;
};

window.Vue.prototype.path = (colloction, hint) => {
  let image = _.find(colloction, ["type", hint]);

  if (image) {
    return image.path;
  }

  return ``;
};

window.Vue.prototype.curLangVal = function(...params) {
  // window.app.lang === 'ar'
  if (true) {
    let attribute = params[0] + "_ar";

    return params[1][attribute];
  }

  if (params[1][params[0] + "_en"]) {
    return params[1][params[0] + "_en"];
  }

  return params[1][params[0]];
};

window.Vue.prototype.trans = (key, replace = {}) => {
  // return '';
  let translation = key
    .split(".")
    .reduce((t, i) => t[i] || null, window.translations);

  for (var placeholder in replace) {
    translation = translation.replace(`:${placeholder}`, replace[placeholder]);
  }

  return translation;
};

window.Vue.prototype.hasMeta = ($meta_key, user) => {
  if (user.user_meta.length == 0) {
    return "";
  }

  let metaObj = _.find(user.user_meta, ["meta_key", $meta_key]);

  if (!metaObj) {
    return "";
  }

  return metaObj.meta_value;

  // $metaObj = _.find(this.user.user_meta, {id: contact.id })
};
