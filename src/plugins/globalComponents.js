import {
  BaseInput,
  Card,
  BaseDropdown,
  BaseButton,
  BaseCheckbox
} from "../components/index";

import btnUpload from "../components/BtnUpload.vue";

import wysiwyg from "../components/Wysiwyg.vue";

import flash from "../components/Flash.vue";

import BounceLoader from "vue-spinner/src/BounceLoader.vue";

import MoonLoader from "vue-spinner/src/MoonLoader.vue";

import ClipLoader from "vue-spinner/src/ClipLoader.vue";

import SyncLoader from "vue-spinner/src/SyncLoader.vue";

import VueButtonSpinner from "vue-button-spinner";

/**
 * You can register global components here and use them as a plugin in your main Vue instance
 */

const GlobalComponents = {
  install(Vue) {
    Vue.component(BaseInput.name, BaseInput);

    Vue.component(Card.name, Card);

    Vue.component(BaseDropdown.name, BaseDropdown);

    Vue.component(BaseButton.name, BaseButton);

    Vue.component(BaseCheckbox.name, BaseCheckbox);

    Vue.component("btn-upload", btnUpload);

    Vue.component("wysiwyg", wysiwyg);

    Vue.component("flash", flash);

    Vue.component("bounce-loader", BounceLoader);

    Vue.component("moon-loader", MoonLoader);

    Vue.component("clip-loader", ClipLoader);

    Vue.component("sync-loader", SyncLoader);

    Vue.component("btn-spinner", VueButtonSpinner);
  }
};

export default GlobalComponents;
