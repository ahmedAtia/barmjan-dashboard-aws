import $ from "jquery";

window.$ = $;

import "bootstrap";

import Vue from "vue";

import App from "./App";

import router from "./router/index";

import { store } from "./store/Store";

import BlackDashboard from "./plugins/blackDashboard";

import Toaster from "v-toaster";

import Form from "./core/Form";

import i18n from "./i18n";
import axios from "axios";

window.axios = axios;

require("./Api");

import "v-toaster/dist/v-toaster.css";

import "trix/dist/trix.css";

import "at.js/dist/css/jquery.atwho.min.css";

if (process.env.VUE_APP_I18N_LOCALE == "ar") {
  require("./assets/css/arabic-skin.css");
}

import VueLazyload from 'vue-lazyload'
Vue.use(VueLazyload)
window.Form = Form;

window.Vue = Vue;

Vue.use(BlackDashboard);

Vue.use(Toaster, { timeout: 6000 });

window._ = require("lodash");

require("./helpers");

window.axios.defaults.baseURL = "https://www.barmj.io";
// window.axios.defaults.baseURL = "http://127.0.0.1:8000";
window.axios.defaults.headers.common = { "X-Requested-With": "XMLHttpRequest" };


// store.getters.access_token
// if (store.getters.loggedIn) {
  axios.defaults.headers.common["Authorization"] =
    "Bearer " +  localStorage.getItem("access_token") ;
// }

// import client from './Api'
// (process.env.NODE_ENV !== 'production') ?

// window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/* eslint-disable no-new */

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
